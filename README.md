Yii2 Upload Files Behavior
==========================
Upload Files Behavior

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist andrewljashenko/yii2-files-upload-behavior "*"
```

or add

```
"andrewljashenko/yii2-files-upload-behavior": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \andrewljashenko\UploadFilesBehavior\AutoloadExample::widget(); ?>```