<?php

namespace andrewljashenko\UploadFilesBehavior;

use yii\base\Behavior;
use yii\base\Exception;
use yii\db\BaseActiveRecord;
use yii\helpers\Url;
use yii\imagine\Image;
use yii\web\UploadedFile;

class UploadFilesBehavior extends Behavior
{
    /**
     * @var string $uploadPath
     *    Upload files directory.
     */
    public $uploadPath = '@common';
    /**
     * @var array $attributes
     *    Array of attributes and settings.
     */
    public $attributes = array();
    /**
     * Initialize Behavior.
     */
    public function init(){
        parent::init();
    }
    /**
     * Connect ActiveRecord Events.
     *
     * @return array
     * @version 1.0
     */
    public function events()
    {
        $event[BaseActiveRecord::EVENT_BEFORE_INSERT] = 'beforeSave';
        $event[BaseActiveRecord::EVENT_BEFORE_UPDATE] = 'beforeSave';
        return $event;
    }
    /**
     * Save files using attributes variable and owner class.
     *
     * @return void
     * @version 1.0
     */
    public function beforeSave()
    {
        // Model class.
        $model = $this->owner;
        if (!empty($this->attributes)) {
            foreach ($this->attributes as $attr) {
                if (!empty($model->{$attr['attribute']})) {
                    // Get files instances for saving.
                    $files = UploadedFile::getInstances($model, $attr['attribute']);
                    if (!empty($files)) {
                        foreach ($files as $file) {
                            // Save file using instance.
                            $model->owner->{$attr['attribute']}[] = $this->saveFile($file, $attr);
                        }
                    }
                }
            }
        }
    }

    /**
     * Save file using file instance and attributes.
     *
     * @param $file
     * @param $attr
     * @return array
     */
    protected function saveFile($file, $attr) {
        $crop = false;
        // Generate upload path.
        $path = Url::to($this->getUploadPath($attr) . DIRECTORY_SEPARATOR . $file->name);
        // Save file and check on image.
        if ($file->saveAs($path)) {
            if (getimagesize($path) && !empty($attr['crop'])) {
                foreach ($attr['crop'] as $value) {
                    if (!empty($value['size']))
                    // Crop image save path.
                    $cropPath = $this->getUploadCropPath($value) . DIRECTORY_SEPARATOR . $file->name;
                    // Crop image using attributes.
                    Image::thumbnail($path, $value['size'][0], $value['size'][1])
                    ->save(\Yii::getAlias($cropPath), ['quality' => $this->getQuality($value)]);
                    $crop[$value['size'][0] . 'x' . $value['size'][1]] = $cropPath;
                }
            }
        }
        return ['path' => $path, 'crop' => $crop];
    }

    /**
     * Return upload path for cropped images.
     *
     * @param $attr
     * @return string
     */
    protected function getUploadCropPath($attr) {
        if (!empty($attr['uploadPath'])) {
            return $attr['uploadPath'];
        }
        return $this->uploadPath . DIRECTORY_SEPARATOR . $attr['size'][0] . 'x' . $attr['size'][1];
    }

    /**
     * Return image quality using attributes.
     *
     * @param $attr
     * @throws Exception
     * @return integer
     */
    protected function getQuality($attr)
    {
        if (!empty($attr['quality'])) {
            if ($attr['quality'] > 100 || $attr['quality'] < 1) {
                throw new Exception("Image quality must be between 1 - 100 ");
            }
            return $attr['quality'];
        }
        return 100;
    }

    /**
     * Return Upload Dir Path.
     *
     * @param $attr
     * @return bool
     */
    protected function getUploadPath($attr)
    {
        $path = !empty($attr['uploadPath']) && !empty($attr['uploadPath']) ?
            $attr['uploadPath'] : $this->uploadPath;
        return !empty($path) ? $path : false;
    }
}
